pub static VERSION: &str = "0.0.1";
pub static GETTEXT_PACKAGE: &str = "echovault";
pub static LOCALEDIR: &str = "/app/share/locale";
pub static PKGDATADIR: &str = "/app/share/echovault";
